# Other releases

Releases for some of my other apps that don't have their own release repository.

## Index
- [Hboard](hboard) ([source code](https://gitlab.com/narektor/hboard))
- [Lyric Timer](lyrictimer) ([source code](https://gitlab.com/narektor/lyric-timer))
- [Batt](batt) ([source code](https://gitlab.com/narektor/batt))
- [Android API tests](apitests):
    - [Quillpad with the default notes role](apitests/udc/quillpad-mod) ([source code](https://gitlab.com/android-api-tests/quillpad))
- [Fake Install](fakeinstall) ([source code](https://gitlab.com/narektor/fake-install))